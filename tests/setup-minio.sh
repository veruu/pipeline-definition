#!/bin/bash
set -euo pipefail

# Setup temporary minio server.

export LOCALMINIO_ENDPOINT=http://localhost:9000
export LOCALMINIO_ACCESS_KEY=cki_temporary
export LOCALMINIO_SECRET_KEY=super_secrete
export LOCALMINIO_ADDRESSING_MODEL=path

if [ ! -x "/tmp/minio" ]; then
    curl --retry 5 -Lso /tmp/minio \
    https://dl.min.io/server/minio/release/linux-amd64/minio
    chmod +x /tmp/minio
fi
rm -rf /tmp/minio-root
mkdir -p /tmp/minio-root
MINIO_ACCESS_KEY="${LOCALMINIO_ACCESS_KEY}" MINIO_SECRET_KEY="${LOCALMINIO_SECRET_KEY}" \
    /tmp/minio server /tmp/minio-root > /dev/null &
LOCALMINIO_PID=$!

# Setup buckets on temporary minio server.

export BUCKET_SOFTWARE="LOCALMINIO|software-bucket|software-path/"
export BUCKET_ARTIFACTS="LOCALMINIO|artifacts-bucket|artifacts-path/"

IFS='|' read -r _ SOFTWARE_BUCKET SOFTWARE_PATH <<< "${BUCKET_SOFTWARE}"
IFS='|' read -r _ ARTIFACTS_BUCKET ARTIFACTS_PATH <<< "${BUCKET_ARTIFACTS}"

for i in $ARTIFACTS_BUCKET $SOFTWARE_BUCKET; do
    AWS_ACCESS_KEY_ID="${LOCALMINIO_ACCESS_KEY}" AWS_SECRET_ACCESS_KEY="${LOCALMINIO_SECRET_KEY}" \
        aws --endpoint-url "${LOCALMINIO_ENDPOINT}" s3api create-bucket --bucket "${i}" > /dev/null
done

# Export variables

echo 'export LOCALMINIO_ENDPOINT="'"$LOCALMINIO_ENDPOINT"'"'
echo 'export LOCALMINIO_ACCESS_KEY="'"$LOCALMINIO_ACCESS_KEY"'"'
echo 'export LOCALMINIO_SECRET_KEY="'"$LOCALMINIO_SECRET_KEY"'"'
echo 'export LOCALMINIO_ADDRESSING_MODEL="'"$LOCALMINIO_ADDRESSING_MODEL"'"'

echo 'export AWS_ACCESS_KEY_ID="'"$LOCALMINIO_ACCESS_KEY"'"'
echo 'export AWS_SECRET_ACCESS_KEY="'"$LOCALMINIO_SECRET_KEY"'"'

echo 'export LOCALMINIO_PID="'"$LOCALMINIO_PID"'"'
echo "trap 'kill $LOCALMINIO_PID' EXIT"

echo 'export BUCKET_SOFTWARE="'"$BUCKET_SOFTWARE"'"'
echo 'export SOFTWARE_BUCKET="'"$SOFTWARE_BUCKET"'"'
echo 'export SOFTWARE_PATH="'"$SOFTWARE_PATH"'"'

echo 'export BUCKET_ARTIFACTS="'"$BUCKET_ARTIFACTS"'"'
echo 'export ARTIFACTS_BUCKET="'"$ARTIFACTS_BUCKET"'"'
echo 'export ARTIFACTS_PATH="'"$ARTIFACTS_PATH"'"'
