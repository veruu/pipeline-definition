#!/bin/bash
set -euo pipefail

function say { echo "$@" | toilet -f mono12 -w 300 | lolcat -f || echo "$@"; }

say "prepare"

# Allow running the tests locally
if [ ! -v CI_PROJECT_DIR ]; then
    export CI_PROJECT_DIR CI_PIPELINE_ID
    CI_PROJECT_DIR=$(pwd)
    CI_PIPELINE_ID=12345
fi
# Add some example projects to test.
export GITLAB_COM_PACKAGES="cki-lib datadefinition skt kpet https://gitlab.com/cki-project/upt"
export GITLAB_COM_DATA_PROJECTS="kpet-db https://gitlab.com/cki-project/kernel-webhooks"
export cki_lib_pip_url="git+https://gitlab.com/cki-project/cki-lib.git@main"
export kpet_db_pip_url="git+https://gitlab.com/cki-project/kpet-db.git@main"
# Additional variables normally supplied via the pipeline.
export SOFTWARE_OBJECT_SUFFIX=python
export SOFTWARE_DIR=${CI_PROJECT_DIR}/software
export VENV_DIR=${SOFTWARE_DIR}/venv
export VENV_BIN=${VENV_DIR}/bin
export VENV_PY3=${VENV_BIN}/python3

export SOFTWARE_OBJECT=pipeline-${CI_PIPELINE_ID}-${SOFTWARE_OBJECT_SUFFIX}.tar.xz

source tests/source_functions.sh

# Verify all software is installed in ${SOFTWARE_DIR}
function verify_installed {
  cd "${SOFTWARE_DIR}"
    echo_green "Verifying directories are present."
    test -d venv

    echo_green "Verifying Python packages."
    for PROJECT in $GITLAB_COM_PACKAGES; do
        if [[ "${PROJECT}" =~ ^http ]]; then
            PROJECT="$(basename "${PROJECT}")"
        fi
        ${VENV_PY3} -m pip show "${PROJECT}"
    done
    for PROJECT in $GITLAB_COM_DATA_PROJECTS; do
        if [[ "${PROJECT}" =~ ^http ]]; then
            PROJECT="$(basename "${PROJECT}")"
        fi
        test -d "${PROJECT}"
    done
  cd "${OLDPWD}"

  echo_green "🥳🤩 SUCCESS!"
}

eval "$(tests/setup-minio.sh)"

# Run the prepare stage
prepare_main
rm -rf "${SOFTWARE_DIR}"
download_software
verify_installed

# Cleanup
rm -rf wheel-verify-venv "${SOFTWARE_DIR}"
