#!/bin/bash
# Add any bash functions needed in the pipeline to this file.
set -euo pipefail

eval "$(sed -n '/AWS-BEGIN/,/AWS-END/p' "${BASH_SOURCE[0]%/*}/../cki_pipeline.yml")"
eval "$(sed -n '/GENERAL-BEGIN/,/GENERAL-END/p' "${BASH_SOURCE[0]%/*}/../cki_pipeline.yml")"
eval "$(sed -n '/KCIDB-BEGIN/,/KCIDB-END/p' "${BASH_SOURCE[0]%/*}/../cki_pipeline.yml")"
eval "$(sed -n '/PREPARE-BEGIN/,/PREPARE-END/p' "${BASH_SOURCE[0]%/*}/../cki_pipeline.yml")"
